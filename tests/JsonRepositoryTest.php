<?php

use  SebSept\JsonRepository\JsonRepository;
use org\bovigo\vfs\vfsStream;

class JsonRepositoryTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var string json file
     */
    private $repository_path;
    protected function setUp()
    {
        $this->repository_path = vfsStream::setup()->url().'/my_storage_file.json';
    }

    /**
     * @test
     */
    public function it_can_be_created()
    {
        JsonRepository::create($this->repository_path);
        $this->assertFileExists($this->repository_path);
    }
    }
}
